<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('post_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('reply_to')->nullable();
            $table->text('comment');
            $table->unsignedBigInteger('approver_id')->nullable();
            $table->timestamp('approved_at')->nullable();
            $table->unsignedBigInteger('disapprover_id')->nullable();
            $table->timestamp('disapproved_at')->nullable();
            $table->timestamps();

            $table->foreign('post_id')
                  ->on('posts')
                  ->references('id');
            
            $table->foreign('user_id')
                  ->on('users')
                  ->references('id');
            
            $table->foreign('reply_to')
                  ->on('comments')
                  ->references('id');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
