<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoryNews = Category::create(['name' => 'News', 'approved_at' => now(), 'approver_id' => 2]);
        $categoryDesign = Category::create(['name' => 'Design', 'approved_at' => now(), 'approver_id' => 2]);
        $categoryTechnology = Category::create(['name' => 'Technology', 'approved_at' => now(), 'approver_id' => 2]);
        $categoryEngineering = Category::create(['name' => 'Engineering', 'approved_at' => now(), 'approver_id' => 2]);

        $tagCustomers = Tag::create(['name' => 'Customers', 'approved_at' => now(), 'approver_id' => 2]);
        $tagDesign = Tag::create(['name' => 'Design', 'approved_at' => now(), 'approver_id' => 2]);
        $tagLaravel = Tag::create(['name' => 'Laravel', 'approved_at' => now(), 'approver_id' => 2]);
        $tagCoding = Tag::create(['name' => 'Coding', 'approved_at' => now(), 'approver_id' => 2]);

        $post1 = Post::create([
            'title' => 'We relocated our office to HOME!',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'images/posts/1.jpg',
            'category_id' => $categoryNews->id,
            'user_id' => 2,
            'published_at' => Carbon::now()->format('Y-m-d'),
            'approved_at' => Carbon::now()->format('Y-m-d'),
            'approver_id' => 2
        ]);

        $post2 = Post::create([
            'title' => 'Corona vaccination drive is on',
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'images/posts/2.jpg',
            'category_id' => $categoryNews->id,
            'user_id' => 2,
            'published_at' => Carbon::now()->format('Y-m-d'),
            'approved_at' => Carbon::now()->format('Y-m-d'),
            'approver_id' => 2
        ]);

        $post3 = Post::create([
            'title' => "Elon musk's tweet pumped shiba inu",
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'images/posts/3.jpg',
            'category_id' => $categoryNews->id,
            'user_id' => 2,
            'published_at' => Carbon::now()->format('Y-m-d'),
            'approved_at' => Carbon::now()->format('Y-m-d'),
            'approver_id' => 2
        ]);

        $post4 = Post::create([
            'title' => "Ipad pro now comes with the all new apple's inhouse M1 chip",
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'images/posts/4.jpg',
            'category_id' => $categoryTechnology->id,
            'user_id' => 3,
            'published_at' => Carbon::now()->format('Y-m-d'),
            'approved_at' => Carbon::now()->format('Y-m-d'),
            'approver_id' => 2
        ]);

        $post5 = Post::create([
            'title' => "Microsoft announced Windows 11 last night and it's a great update!",
            'excerpt' => Factory::create()->sentence(rand(10, 18)),
            'content' => Factory::create()->paragraphs(rand(3, 7), true),
            'image' => 'images/posts/5.jpg',
            'category_id' => $categoryTechnology->id,
            'user_id' => 3,
            'published_at' => Carbon::now()->format('Y-m-d'),
            'approved_at' => Carbon::now()->format('Y-m-d'),
            'approver_id' => 2
        ]);

        // Creating Entries for bridging table: post_tag
        $post1->tags()->attach([$tagCustomers->id, $tagDesign->id]);
        $post2->tags()->attach([$tagCustomers->id]);
        $post3->tags()->attach([$tagDesign->id]);
        $post4->tags()->attach([$tagCoding->id]);
        $post5->tags()->attach([$tagCoding->id, $tagDesign->id, $tagLaravel->id]);

    }
}
