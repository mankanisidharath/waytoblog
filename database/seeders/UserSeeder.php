<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'member-approver',
            'email' => 'member-approver@waytoblog.com',
            'password' => Hash::make("password"),
            'role' => User::ADMIN
        ]);
        User::create([
            'name' => 'Sidharath Mankani',
            'email' => 'mankanisidharath@gmail.com',
            'password' => Hash::make("password"),
            'role' => User::ADMIN
        ]);
        User::create([
            'name' => 'Mankani Sidharath',
            'email' => 'smankani44@gmail.com',
            'password' => Hash::make("password")
        ]);
    }
}
