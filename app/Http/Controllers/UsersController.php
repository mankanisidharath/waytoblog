<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\DeleteUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','admin'])->except('edit', 'update', 'trash', 'subscribe', 'renewSubscription');
        $this->middleware(['validateUser'])->only('edit', 'update', 'trash', 'subscribe', 'renewSubscription');
    }

    public function index(){
        $users = User::paginate(10);
        return view('users.index',compact(['users']));
    }

    public function makeAdmin(User $user){
        $user->update(['role' => User::ADMIN]);
        session()->flash('success', $user->name . ' has been assigned admin role!');
        return redirect(route('users.index'));
    }

    public function revokeAdmin(User $user){
        $user->update(['role' => User::AUTHOR]);
        session()->flash('success', $user->name . ' has been revoked admin role!');
        return redirect(route('users.index'));
    }

    public function create()
    {
        return view('users.create');
    }

    
    public function store(CreateUserRequest $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);
        session()->flash('success', 'User Created Successfully!');
        return redirect(route('users.index'));
    }

    public function edit(User $user)
    {
        return view('users.edit', compact(['user']));
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        if($request->password != '') {
            $password = Hash::make($request->password);
        } else {
            $password = $user->password;
        }
        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $password
        ]);
        session()->flash('success', 'User Updated Successfully!');
        return redirect(route('users.index'));
    }

    public function destroy($id)
    {
        $user = User::onlyTrashed()->findOrFail($id);
        $user->forceDelete();
        session()->flash('success', 'User Deleted successfully!');
        return redirect(route('users.trashed'));
    }
    
    public function trash(DeleteUserRequest $request, User $user)
    {
        $user->delete();
        return redirect(route('home'));
    }

    public function trashed()
    {
        $users = User::onlyTrashed()->paginate(10);
        return view('users.trashed', compact(['users']));
    }

    public function restore($id)
    {
        $trashedUser = User::onlyTrashed()->findOrFail($id);
        $trashedUser->restore();
        session()->flash('success', 'User Restored Successfully!');
        return redirect(route('users.index'));
    }

    public function subscribe(User $user)
    {
        $user->subscription()->create([
            'started_at' => now(),
            'ends_at' => now()->addDays(1)
        ]);
        return redirect(route('subscription'));
    }

    public function renewSubscription(User $user)
    {
        $user->subscription->update([
            'started_at' => now(),
            'ends_at' => now()->addDays(1)
        ]);
        return redirect(route('subscription'));
    }

}