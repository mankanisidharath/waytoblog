<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCommentRequest;
use App\Models\Comment;

class CommentsController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('admin')->only('approve', 'disApprove');
    }

    public function index()
    {
        if (auth()->user()->isAdmin()) {
            $comments = Comment::commented()->paginate(10);
            return view('comments.index', compact('comments'));
        } else {
            $comments = Comment::commented()->where('user_id', auth()->id())->paginate(10);
            return view('comments.index', compact('comments'));
        }
    }

    public function store(StoreCommentRequest $request)
    {
        Comment::create([
            'user_id' => auth()->id(),
            'post_id' => $request->post_id,
            'comment' => $request->comment,
            'reply_to' => $request->reply_to
        ]);
        session()->flash('posted', 'Your comment will be posted as soon as it is approved!');
        return redirect(route('blogs.show', $request->post_id));
    }

    public function approve(Comment $comment) {
        $comment->update([
            'approver_id' => auth()->id(),
            'approved_at' => now(),
            'disapprover_id' => null,
            'disapproved_at' => null,
        ]);
        session()->flash('success', 'Comment Approved Successfully');
        return redirect(route('comments.index'));
    }

    public function disApprove(Comment $comment) {
        $comment->update([
            'disapprover_id' => auth()->id(),
            'disapproved_at' => now(),
            'approver_id' => null,
            'approved_at' => null
        ]);
        session()->flash('success', 'Comment Disapproved Successfully');
        return redirect(route('comments.index'));
    }

}
