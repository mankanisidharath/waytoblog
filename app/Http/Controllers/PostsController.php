<?php

namespace App\Http\Controllers;

use App\Http\Requests\DisapprovePostRequest;
use App\Http\Requests\posts\CreatePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Http\JsonResponse;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['auth', 'admin'])->only('approve', 'disApprove');
        $this->middleware(['verifyCategoriesCount'])->only('create', 'store');
        $this->middleware(['verifyTagsCount'])->only('create', 'store');
        $this->middleware(['validateAuthor'])->only('edit', 'update', 'trash', 'restore');
    }

    public function index()
    {
        if(auth()->user()->isAdmin()) {
            $posts = Post::publishedAndScheduled()->paginate(3);
        }else {
            $posts = Post::userPosts()->publishedAndScheduled()->paginate(3);
        }
        return view('posts.index', compact(['posts']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::approved()->get();
        $tags = Tag::approved()->get();
        return view('posts.create', compact(['categories', 'tags']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePostRequest $request)
    {
        $isMember = auth()->user()->isMember();
        $data = $request->only(['title', 'excerpt', 'content', 'category_id', 'tags[]']);
        if(isset($request->submit)) {
            $data['published_at'] = now();
            if($isMember) {
                if($request->publish == 'future') {
                    $data['published_at'] = $request->published_at;
                }
                $data['approver_id'] = User::MEMBER_APPROVER;
                $data['approved_at'] = $request->published_at;
            } else {
                $data['approver_id'] = null;
                $data['approved_at'] = null;
            }
        } else {
            $data['published_at'] = null;
        }
        $data['user_id'] = auth()->id();
        $image = $request->file('image')->store('images/posts');
        $data['image'] = $image;
        $post = Post::create($data);
        $post->tags()->attach($request->tags);
        $isMember? session()->flash('success', 'Your Blog is live!') : session()->flash('success', 'Your Blog is uploaded and will be published shortly!');
        if(isset($request->submit)) {
            return redirect(route('posts.index'));
        } else {
            return redirect(route('posts.drafted'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = Category::approved()->get();
        $tags = Tag::approved()->get();
        return view('posts.edit', compact(['post', 'categories', 'tags']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, Post $post)
    {
        $isMember = auth()->user()->isMember();
        $data = $request->only(['title', 'excerpt', 'content', 'category_id']);
        if(isset($request->submit)) {
            $data['published_at'] = now();
            if($isMember) {
                if($request->publish == 'future') {
                    $data['published_at'] = $request->published_at;
                }
                /**
                 * If the disapproved_at is set, it means that a (pro member)author's post is disapproved, then even though the author is a member, we don't wanna give auto-member-approval on this post. hence, the below code;
                 */
                if(!isset($post->disapproved_at)) {
                    $data['approver_id'] = User::MEMBER_APPROVER;
                    $data['approved_at'] = $request->published_at;
                } else {
                    $data['approver_id'] = null;
                    $data['approved_at'] = null;
                    session()->flash('success', 'Your post is updated but needs approval as it had been disapproved before!');
                }
            } else { // If it is not member, the author will need approval from an admin to get the post li
                $data['approver_id'] = null;
                $data['approved_at'] = null;
            }
        } else {
            $data['published_at'] = null;
            session()->flash('success', 'Your post is marked as draft!');
        }
        if($request->hasFile('image')) {
            $image = $request->image->store('images/posts');
            $data['image'] = $image;
            $post->deleteImage();
        }

        $data['user_id'] = auth()->id();
        $data['disapprover_id'] = null;
        $data['disapproved_at'] = null;
        $data['disapprover_comment'] = null;
        $post->update($data);

        $post->tags()->sync($request->tags);

        if(!session()->exists('success')) {
            $isMember? session()->flash('success', 'Your Blog is published!') : session()->flash('success', 'Your Blog is updated and will be published shortly!');
        }

        if(isset($request->submit)) {
            return redirect(route('posts.index'));
        } else {
            return redirect(route('posts.drafted'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::onlyTrashed()->findOrFail($id);
        $post->deleteImage();
        $post->forceDelete();
        session()->flash('success', 'Post Deleted successfully!');
        return redirect(route('posts.trashed'));
    }

    public function trashed()
    {
        if(auth()->user()->isAdmin()) {
            $trashed = Post::onlyTrashed()->paginate(10);
        } else {
        $trashed = Post::userPosts()->onlyTrashed()->paginate(10);
        }
        return view('posts.trashed', ['posts' => $trashed]);
    }

    public function drafted()
    {
        $drafted = Post::userPosts()->drafted()->paginate(5);
        return view('posts.drafted', ['posts' => $drafted]);
    }

    public function publishDrafted(Post $post)
    {
        $data['published_at'] = now();
        if(auth()->user()->isMember()) {
            $data['approved_at'] = now();
            $data['approver_id'] = User::MEMBER_APPROVER;
        }

        $post->update($data);
        session()->flash('success', 'Post Published Successfully!');
        return redirect(route('posts.index'));
    }

    public function trash(Post $post)
    {
        $post->update([
            'disapprover_id' => null,
            'disapproved_at' => null,
            'approver_id' => null,
            'approved_at' => null,
            'disapprover_comment' => null
        ]);
        $post->delete();
        session()->flash('success', 'Post Trashed successfully!');
        return redirect(route('posts.trashed'));
    }

    public function restore($id)
    {
        $trashedPost = Post::onlyTrashed()->findOrFail($id);
        $trashedPost->restore();
        session()->flash('success', 'Post Restored Successfully!');
        return redirect(route('posts.index'));
    }

    public function approve(Post $post) {
        $post->update([
            'approver_id' => auth()->id(),
            'approved_at' => now(),
            'disapprover_id' => null,
            'disapproved_at' => null,
            'disapprover_comment' => null,
        ]);
        session()->flash('success', 'Post Approved Successfully');
        return redirect(route('posts.index'));
    }

    public function disApprove(DisapprovePostRequest $request, Post $post) {
        $post->update([
            'disapprover_id' => auth()->id(),
            'disapproved_at' => now(),
            'disapprover_comment' => $request->comment,
            'approver_id' => null,
            'approved_at' => null
        ]);
        session()->flash('success', 'Post Disapproved Successfully');
        return redirect(route('posts.index'));
    }

    public function getPostsJson(): JsonResponse
    {
        /**
         * Data to be returned: -
                title
                date
                authorName
                rating
                views
                description
         */

        /*
        return Post::with('author')->get()->map->only(['id', 'author', 'title', 'published_at', 'excerpt'])->map(function ($post) {
            $post['author'] = [
                "name" => $post['author']->name,
                "avatar" => $post['author']->gravatar_image
            ];
            $post['date'] = $post['published_at']->diffForHumans();
            $post['image'] = asset('storage/images/posts/1.jpg');
            $post['description'] = $post['excerpt'];
            $post['views'] = 120;
            $post['rating'] = 5.0;
        })->toJson();
        */

        $posts = Post::with('author')->publishedAndScheduled()->approved()->get();
        $blogData = [
            'data' => [],
        ];
        foreach($posts as $post) {
            $temp['id'] = $post->id;
            $temp['author'] = [
                "name" => $post->author->name,
                "avatar" => $post->author->gravatar_image
            ];
            $temp['title'] = $post->title;
            $temp['date'] = $post->published_at->diffForHumans();
            $temp['image'] = "/storage/{$post->image}";
            $temp['description'] = $post->content;
            $temp['views'] = 120;
            $temp['rating'] = 5.0;
            $blogData['data'][] = $temp;
        }
        return new JsonResponse($blogData);
    }

}
