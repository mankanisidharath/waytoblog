<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['admin'])->only('edit', 'update', 'destroy', 'approve', 'disApprove');
    }
    
    public function index()
    {
        $categories = Category::paginate(5);
        return view('categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request)
    {
        //  1. Validation is already done with the help of form request
        //  store the data in db
        Category::create([
            'name' => $request->name
        ]);
        session()->flash('success', 'Category created successfully!');
        // session()->flash('error', "Couldn't create Category!"); // pending here exception handling as create throws exception if it fails
        // return to index
        return redirect(route('categories.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('categories.edit', ['category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $category->update([
            'name'=> $request->name,
            'approver_id' => null,
            'approved_at' => null,
            'disapprover_id' => null,
            'disapproved_at' => null
        ]);
        session()->flash('success', 'Category Updated Successfully!');
        return redirect(route('categories.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        if($category->posts->count() > 0) {
            session()->flash('error', 'This Category cannot be deleted!');
            return redirect(route('categories.index'));
        }
        $category->delete();
        session()->flash('success', 'Category Deleted Successfully!');
        return redirect(route('categories.index'));
    }

    public function approve(Category $category) {
        $category->update([
            'approver_id' => auth()->id(),
            'approved_at' => now(),
            'disapprover_id' => null,
            'disapproved_at' => null
        ]);
        session()->flash('success', 'Category Approved Successfully');
        return redirect(route('categories.index'));
    }

    public function disApprove(Category $category) {
        if($category->posts->count() > 0) {
            session()->flash('error', 'This Category cannot be disapproved!');
            return redirect(route('categories.index'));
        }
        $category->update([
            'disapprover_id' => auth()->id(),
            'disapproved_at' => now(),
            'approver_id' => null,
            'approved_at' => null
        ]);
        session()->flash('success', 'Category Disapproved Successfully');
        return redirect(route('categories.index'));
    }

}
