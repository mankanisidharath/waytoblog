<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index()
    {
        $posts = Post::search()
            ->latest('approved_at')
            ->published()
            ->approved()
            ->simplePaginate(3);
        $tags = Tag::approved()->get();
        $categories = Category::approved()->get();

        return view('blogs.index', compact(['posts', 'tags', 'categories']));
    }

    public function show(Post $post)
    {
        $categories = Category::approved()->get();
        $tags = Tag::approved()->get();
        return view('blogs.post', compact(['post', 'tags', 'categories']));
    }

    public function categories(Category $category)
    {
        $posts = $category->posts()
                ->search()
                ->published()
                ->latest('published_at')
                ->simplePaginate(10);

        $tags = Tag::approved()->get();
        $categories = Category::approved()->get();
        return view('blogs.index', compact(['posts', 'tags', 'categories']));
    }

    public function tags(Tag $tag)
    {
        $posts = $tag->posts()
                ->search()
                ->published()
                ->latest('published_at')
                ->simplePaginate(10);
        $tags = Tag::approved()->get();
        $categories = Category::approved()->get();
        return view('blogs.index', compact(['posts', 'tags', 'categories']));
    }

    public function preview(Post $post)
    {
        return view('blogs.preview', compact(['post']));
    }

    public function subscription()
    {
        return view('subscription');
    }

}
