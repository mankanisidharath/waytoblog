<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTagRequest;
use App\Http\Requests\UpdateTagRequest;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('admin')->only('edit', 'update', 'destroy', 'approve', 'disApprove');
    }

    public function index()
    {
        $tags = Tag::paginate(5);
        return view('tags.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTagRequest $request)
    {
        Tag::create([
            'name' => $request->name
        ]);
        session()->flash('success', 'Tag created successfully!');
        return redirect(route('tags.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        return view('tags.edit', ['tag' => $tag]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTagRequest $request, Tag $tag)
    {
        $tag->update([
            'name'=> $request->name,
            'approver_id' => null,
            'approved_at' => null,
            'disapprover_id' => null,
            'disapproved_at' => null
        ]);
        session()->flash('success', 'Tag Updated Successfully!');
        return redirect(route('tags.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        if($tag->posts->count() > 0) {
            session()->flash('error', 'This Tag cannot be deleted!');
            return redirect(route('tags.index'));
        }
        $tag->delete();
        session()->flash('success', 'Tag Deleted Successfully!');
        return redirect(route('tags.index'));
    }

    public function approve(Tag $tag) {
        $tag->update([
            'approver_id' => auth()->id(),
            'approved_at' => now(),
            'disapprover_id' => null,
            'disapproved_at' => null
        ]);
        session()->flash('success', 'Tag Approved Successfully');
        return redirect(route('tags.index'));
    }

    public function disApprove(Tag $tag) {
        if($tag->posts->count() > 0) {
            session()->flash('error', 'This Tag cannot be disapproved!');
            return redirect(route('tags.index'));
        }
        $tag->update([
            'disapprover_id' => auth()->id(),
            'disapproved_at' => now(),
            'approver_id' => null,
            'approved_at' => null
        ]);
        session()->flash('success', 'Tag Disapproved Successfully');
        return redirect(route('tags.index'));
    }
}
