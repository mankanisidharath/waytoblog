<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class VerifyPreviewPost
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(auth()->check()) {
            if(!auth()->user()->isAdmin()) {
                if(!$request->post->isApproved()) {
                    if($request->post->user_id != auth()->id()) {
                        return abort(401);
                    }
                }
            }
        } elseif(!$request->post->isApproved()) {
            return abort(401);
        }
        
        return $next($request);
    }
}
