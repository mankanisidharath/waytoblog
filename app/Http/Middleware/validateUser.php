<?php

namespace App\Http\Middleware;

use App\Http\Requests\DeleteUserRequest;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class validateUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if(is_object($request->user)) {
            if($request->user->id != auth()->id()) {
                return abort(401);
            }

        } else if (is_numeric($request->user)) {
            if($request->user != auth()->id()) {
                return abort(401);
            }

        }
        return $next($request);
    }
}
