<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function posts()
    {
        return $this->belongsToMany(Post::class)->withTimestamps();
    }

    public function scopeApproved($query) {
        return $query->where('approved_at', '!=', null);
    }

    public function isApproved(): bool {
        return isset($this->approved_at);
    }

    public function isPending(): bool {
        return !(isset($this->approved_at) xor isset($this->disapproved_at));
    }
}
