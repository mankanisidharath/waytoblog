<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Post extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $guarded = ['id'];
    protected $dates = ['published_at', 'approved_at', 'disapproved_at'];

    // Accessor
    public function getImagePathAttribute()
    {
        return 'storage/'.$this->image;
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class)->withTimestamps();
    }

    public function comments()
    {
        return $this->hasMany(Comment::class)->commented()->approved();
    }

    public function hasTag(int $tag_id): bool
    {
        return in_array($tag_id, $this->tags->pluck('id')->toArray());
    }

    public function isApproved(): bool {
        return isset($this->approved_at);
    }

    public function isPending(): bool {
        return !(isset($this->approved_at) xor isset($this->disapproved_at));
    }

    public function deleteImage()
    {
        Storage::delete($this->image);
    }

    // Accessors
    public function getApproverAttribute() {
        return User::findOrFail($this->approver_id);
    }

    public function getDisapproverCommentAttribute() {
        // return $this->disapprover_comment;
        return $this->attributes['disapprover_comment'];
    }

    // Query Scopes

    public function scopePublishedAndScheduled($query)
    {
        return $query->where('published_at', '!=', null);
    }

    public function scopePublished($query)
    {
        return $query->where('published_at', '<=', now());
    }

    public function scopeDrafted($query)
    {
        return $query->where('published_at', null);
    }

    public function scopeSearch($query)
    {
        $search = request('search');
        if($search) {
            return $query->where('title', 'LIKE', "%$search%")
                    ->orWhere('excerpt', 'LIKE', "%$search%");
        }
        return $query;
    }

    public function scopeApproved($query) {
        return $query->where('approved_at', '!=', null);
    }

    public function scopeUserPosts($query) {
        $query->where('user_id', auth()->id());
    }

}
