<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function scopeApproved($query) {
        return $query->where('approved_at', '!=', null);
    }

    public function isApproved(): bool {
        return isset($this->approved_at);
    }

    public function isPending(): bool {
        return !(isset($this->approved_at) xor isset($this->disapproved_at));
    }

}
