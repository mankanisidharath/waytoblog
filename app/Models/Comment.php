<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    
    public function post() {
        return $this->belongsTo(Post::class);
    }

    public function author() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function replies() {
        return $this->hasMany(Comment::class, 'reply_to');
    }

    public function approvedReplies() {
        return $this->hasMany(Comment::class, 'reply_to')->approved();
    }

    public function isApproved(): bool {
        return isset($this->approved_at);
    }

    public function isPending(): bool {
        return !(isset($this->approved_at) xor isset($this->disapproved_at));
    }

    // Accessors
    public function getApproverAttribute() {
        return User::find($this->approver_id);
    }

    // Query Scope
    public function scopeApproved($query) {
        return $query->where('approved_at', '!=', null);
    }

    public function scopeCommented($query) {
        return $query->where('reply_to', null);
    }

}
