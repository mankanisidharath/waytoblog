<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Thomaswelton\LaravelGravatar\Facades\Gravatar;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes;

    const MEMBER_APPROVER = 1;
    const ADMIN = 'admin';
    const AUTHOR = 'author';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function subscription()
    {
        return $this->hasOne(Subscription::class);
    }

    public function isMember(): bool
    {
        return $this->subscription && now()->lessThan($this->subscription->ends_at);
    }

    public function wasMember(): bool
    {
        return $this->subscription && now()->greaterThanOrEqualTo($this->subscription->ends_at);
    }
    
    public function isAdmin(): bool
    {
        return $this->role === self::ADMIN;
    }

    function getGravatarImageAttribute() {
	    return Gravatar::src($this->email, 80);
    }

}
