@extends('layouts.admin-panel.app')

@section('title', 'WayToBlog')

@section('content')

    <div class="font-sans text-4xl antialiased font-semibold tracking-wide flex">{{auth()->user()->name}}<span class="badge ml-2 self-center px-2 py-2 rounded-full text-sm text-white bg-red-500">{{auth()->user()->role}}</span></div>
    <div class="font-sans text-base antialiased font-normal tracking-wide text-gray-400">{{auth()->user()->email}}</div>

    <ul class="list-group list-group-flush mt-5">
        <li class="list-group-item font-sans text-xl antialiased font-normal">Posts Created: {{auth()->user()->posts->count()}}</li>
        <li class="list-group-item font-sans text-xl antialiased font-normal">Categories Created: {{auth()->user()->posts->count()}}</li>
        <li class="list-group-item font-sans text-xl antialiased font-normal">Tags Created: {{auth()->user()->posts->count()}}</li>
    </ul>

    <div class="flex">
        <a href="{{ route('users.edit', auth()->id()) }}" class="font-sans text-xl antialiased font-normal tracking-wide mt-5 px-4 py-2 btn text-red-500 border-red-500 hover:bg-red-500 hover:text-white flex">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2 align-self-center" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
            </svg>
            Edit Profile
        </a>
        <a href="{{ route('posts.create') }}" class="font-sans text-xl antialiased font-normal tracking-wide mt-5 ml-3 px-4 py-2 btn bg-red-500 text-gray-50 border-red-500 hover:bg-white hover:text-red-500 flex">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 align-self-center" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
            </svg>
            Add Blogs</a>
    </div>


@endsection
