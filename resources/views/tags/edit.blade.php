@extends('layouts.admin-panel.app')

@section('title', 'WayToBlog')

@section('content')

    <div class="card">
        <div class="card-header h2">Edit a Tag</div>
        <div class="card-body">
            <form action="{{ route('tags.update', $tag->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name" class="@error('name') text-danger @enderror" >Name</label>
                    <input  type="text"
                            class="form-control @error('name') is-invalid @enderror"
                            id="name"
                            name="name"
                            value="{{ old('name', $tag->name) }}"
                            placeholder="Enter tag Name">
                    @error('name')
                        <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <button type="submit" class="btn btn-outline-primary float-right px-4">Edit</button>
            </form>
        </div>
    </div>

@endsection
