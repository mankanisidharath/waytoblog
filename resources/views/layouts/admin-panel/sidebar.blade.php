<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar">
      <div class="position-sticky pt-3">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link transition-colors duration-300 hover:text-red-500 @if(request()->routeIs('dashboard')) text-red-500 @endif" aria-current="page" href="{{ route('dashboard') }}">
              Dashboard
            </a>
          </li>
        @if(auth()->user()->isAdmin())
            <li class="nav-item">
                <a class="nav-link transition-colors duration-300 hover:text-red-500 @if(request()->routeIs('users.index')) text-red-500 @endif" href="{{ route('users.index') }}">
                Users
                </a>
            </li>
        @endif
          <li class="nav-item">
            <a class="nav-link transition-colors duration-300 hover:text-red-500 @if(request()->routeIs('posts.index')) text-red-500 @endif" href="{{ route('posts.index') }}">
              Posts
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link transition-colors duration-300 hover:text-red-500 @if(request()->routeIs('categories.index')) text-red-500 @endif" href="{{ route('categories.index') }}">
              Categories
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link transition-colors duration-300 hover:text-red-500 @if(request()->routeIs('tags.index')) text-red-500 @endif" href="{{ route('tags.index') }}">
              Tags
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link transition-colors duration-300 hover:text-red-500 @if(request()->routeIs('comments.index')) text-red-500 @endif" href="{{ route('comments.index') }}">
              Comments
            </a>
          </li>
        </ul>

        <h5 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          <span>Trash</span>
        </h5>
        <ul class="nav flex-column mb-2">
          <li class="nav-item">
            <a class="nav-link transition-colors duration-300 hover:text-red-500 @if(request()->routeIs('posts.trashed')) text-red-500 @endif" href="{{ route('posts.trashed') }}">
              Posts
            </a>
          </li>
          @if(auth()->user()->isAdmin())
            <li class="nav-item">
                <a class="nav-link transition-colors duration-300 hover:text-red-500 @if(request()->routeIs('users.trashed')) text-red-500 @endif" href="{{ route('users.trashed') }}">
                Users
                </a>
            </li>
          @endif
        </ul>

        <h5 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          <span>Drafts</span>
        </h5>
        <ul class="nav flex-column mb-2">
          <li class="nav-item">
            <a class="nav-link transition-colors duration-300 hover:text-red-500 @if(request()->routeIs('posts.drafted')) text-red-500 @endif" href="{{ route('posts.drafted') }}">
              Posts
            </a>
          </li>
        </ul>
      </div>
    </nav>