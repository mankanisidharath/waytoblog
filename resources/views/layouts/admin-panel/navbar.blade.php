<nav class="navbar navbar-expand-lg bg-white shadow-lg">
    <div class="container">
       <div class="navbar-brand">
           <a class="nav-link flex logo-text align-items-center" href="{{ route('home') }}" style="font-family: 'Pacifico', cursive;font-size: 24px;"><img class="mr-2" src="{{ asset('frontend/assets/img/logo/logo-default.svg') }}"> WayToBlog @if(auth()->check() && auth()->user()->isMember()) Pro @endif</a>
       </div>
       <div class="collapse navbar-collapse" id="navbarSupportedContent">
           <ul class="navbar-nav ml-auto">
                @if(!auth()->user()->isMember())
                    <li>
                        <a href="{{ route('subscription') }}" class="nav-link mr-3 flex align-baseline hover:text-red-500 transition-colors duration-300">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 10h18M7 15h1m4 0h1m-7 4h12a3 3 0 003-3V8a3 3 0 00-3-3H6a3 3 0 00-3 3v8a3 3 0 003 3z" />
                            </svg>
                            Subscribe
                        </a>
                    </li>
                @endif
                <li>
                    <a href="{{ route('home') }}" class="nav-link mr-3 flex align-baseline hover:text-red-500 transition-colors duration-300">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6" />
                        </svg>
                        Home
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle hover:text-red-500 transition-colors duration-300" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{ auth()->user()->name }}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a href="{{ route('users.edit', auth()->user()->id) }}" class="dropdown-item">Edit profile</a>
                        <a href="#deleteUserModal" data-toggle="modal" class="dropdown-item">Delete account</a>
                        <form action="{{ route('logout') }}" method="POST">
                            @csrf
                            <input type="submit" class="dropdown-item" value="Logout">
                        </form>
                    </div>
                </li>
           </ul>
       </div>
   </div>
</nav>
