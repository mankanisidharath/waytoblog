<!doctype html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Google Font -->
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Pacifico&display=swap" rel="stylesheet">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

		<!-- Custom CSS -->
		<link rel="stylesheet" href="{{ asset('css/app.css') }}">
		@yield('page-level-styles')

		<title>@yield('title')</title>
	</head>
	<body>

		@include('layouts.admin-panel.navbar')

		@error('delete')
			{{ session()->flash('error', "Delete Account Failed - Entered value was not correct!") }}
		@enderror

		<div class="container-fluid py-5">
				<div class="row">
						<div class="col-md-2">
								@include('layouts.admin-panel.sidebar')
						</div>
						<div class="col-md-10">
								<div class="card shadow p-5">
									@yield('errors')
									@include('layouts.partials._message')
									@yield('content')
								</div>
						</div>
				</div>
		</div>

		<!-- Delete User Modal -->
		<!-- Modal -->
		<div class="modal fade" id="deleteUserModal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Delete your account!</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form action="{{ route('users.trash', auth()->id()) }}" id="deleteUserForm" method="POST" autocomplete="off">
					@csrf
					@method('DELETE')
					<div class="modal-body">
						<p><span class="text-danger">Your account will be deleted.</span> you may or may not be able to recover it. If you still wish to proceed, type 'delete' below to delete your account.</p>
						<input type="text" name="delete" id="delete" class="form-control mt-2 is-invalid text-danger" placeholder="Type delete here">
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<input type="submit" value="">
					</div>
				</form>
			</div>
		</div>
		</div>

		<!-- Optional JavaScript -->
		<!-- jQuery first, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
		<script>
			$('#DeleteUserForm').submit(function(event) {
				let deleteText = $('$delete').attr('innerHTML');
				console.log(deleteText);
				if(deleteText != 'delete') {
					event.preventDefault();
				}
			});
			$('#deleteUserModal').on('hidden.bs.modal', function() {
				document.querySelector('#delete').value = "";
			});
		</script>
		@yield('page-level-scripts')
	</body>
</html>
