<!-- Navigation Area
===================================== -->
<nav class="navbar navbar-pasific navbar-mp megamenu navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                <i class="fa fa-bars"></i>
            </button>
        
            <a class="navbar-brand page-scroll" href="{{route('home')}}">
                <img src="{{ asset("frontend/assets/img/logo/logo-default.svg") }}" width="40px" height="40px" alt="logo">
                WayToBlog @if(auth()->check() && auth()->user()->isMember()) Pro @endif
            </a>
        </div>

        <div class="navbar-collapse collapse navbar-main-collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ route('home') }}" class="dropdown-toggle color-light">Home</a>
                </li>
                @if (auth()->check())
                    <li>
                        <a href="{{ route('dashboard') }}" class="dropdown-toggle color-light">Dashboard</a>
                    </li>
                @else
                    <li>
                        <a href="{{ route('login') }}" class="dropdown-toggle color-light">Login</a>
                    </li>
                    <li>
                        <a href="{{ route('register') }}" class="dropdown-toggle color-light">Register</a>
                    </li>
                @endif
                @if(auth()->check())
                    @if(!auth()->user()->isMember()))
                        <li>
                            <a href="{{ route('subscription') }}" class=" color-light">Subscribe</a>
                        </li>
                    @endif
                @else
                    <li>
                        <a href="{{ route('subscription') }}" class=" color-light">Subscribe</a>
                    </li>
                @endif
            </ul>

        </div>
    </div>
</nav>