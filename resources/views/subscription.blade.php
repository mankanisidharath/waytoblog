<!DOCTYPE html>
<html lang="en">
<head>
    <title>Subscribe</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta charset="utf-8">
    <meta name="author" content="John Doe">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

    <!-- Favicons -->
    <link rel="shortcut icon" href="{{ asset('frontend/assets/img/favicon.ico') }}" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('frontend/assets/img/logo/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('frontend/assets/img/logo/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('frontend/assets/img/logo/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('frontend/assets/img/logo/site.webmanifest') }}">


    <!-- Load Core CSS
    =====================================-->
    <link rel="stylesheet" href="{{ asset("frontend/assets/css/core/bootstrap-3.3.7.min.css") }}">
    <link rel="stylesheet" href="{{ asset("frontend/assets/css/core/animate.min.css") }}">

    <!-- Load Main CSS
    =====================================-->
    <link rel="stylesheet" href="{{ asset("frontend/assets/css/main/main.css") }}">
    <link rel="stylesheet" href="{{ asset("frontend/assets/css/main/setting.css") }}">
    <link rel="stylesheet" href="{{ asset("frontend/assets/css/main/hover.css") }}">


    <link rel="stylesheet" href="{{ asset("frontend/assets/css/color/pasific.css") }}">
    <link rel="stylesheet" href="{{ asset("frontend/assets/css/icon/font-awesome.css") }}">
    <link rel="stylesheet" href="{{ asset("frontend/assets/css/icon/et-line-font.css") }}">

    <link rel="stylesheet" href="{{ asset("css/app.css") }}">

    <!-- Load JS
    HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries
    WARNING: Respond.js doesn't work if you view the page via file://
    =====================================-->

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
    <body id="topPage" data-spy="scroll" data-target=".navbar" data-offset="100">

        <!-- Page Loader
        ===================================== -->
        <div id="pageloader" class="bg-grad-animation-1">
            <div class="loader-item">
                <img src="{{ asset("frontend/assets/img/other/oval.svg") }}" alt="page loader">
            </div>
        </div>

        <a href="#page-top" class="go-to-top">
            <i class="fa fa-long-arrow-up"></i>
        </a>

        @include('layouts.frontend.partials._navigation')
        
        <header class="pt100 pb100 parallax-window-2" data-parallax="scroll" data-speed="0.5" data-image-src="{{ asset('frontend/assets/img/bg/new-img-bg-4.jpg')}}" data-positiony="1000">
		<div class="intro-body text-center">
			<div class="container">
				<div class="row">
					<div class="col-md-12 pt50">
						<div class="h1 brand-heading font-montserrat text-uppercase color-light" data-in-effect="fadeInDown">
							Way To Blog
							<small class="color-light alpha7">Complete Blogging Application</small>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

        <!-- Blog Area 
        ===================================== -->
        <div id="blog" class="my-32">
            <div class="container">
                @if(auth()->user()->isMember())
                    <div class="rounded-lg border border-gray-400 border-opacity-50 flex items-center justify-center">
                        <div class="font-sans text-6xl antialiased font-bold font-wider leading-normal text-red-500 capitalize p-8 m-4">Congratulations! You are subscribed to WayToBlog Pro.</div>
                    </div>
                @else
				    <div class="rounded-lg border border-gray-400 border-opacity-50">
                        <div class="font-sans text-6xl antialiased font-bold font-wider leading-normal text-red-500 capitalize p-8 m-4 pb-0 mb-0">Get Feature rich WayToBlog Pro.</div>
                        <div class="font-sans text-4xl antialiased font-bold font-wider leading-normal text-gray-500 text-opacity-70 capitalize ml-12 mb-5">Join the community.</div>
                        <ul class="list-disc list-inside text-4xl m-8 ml-14">
                            <li class="text-gray-800 my-4">No Approvals Required. Publish live posts in just a click!</li>
                            <li class="text-gray-800 my-4">Get Future Auto-Publishing on Posts.</li>
                            <li class="text-gray-800 my-4">Create your own categories and tags instantly.</li>
                            <li class="text-gray-800 my-4">Priority support :)</li>
                        </ul>
                        <div class="m-10">
                            @if(auth()->user()->wasMember())
                                <form action="{{ route('users.renewSubscription', auth()->id()) }}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <button type="submit" class="button button-pasific hover-ripple-out text-3xl antialiased font-normal tracking-wide mt-5 px-6 py-4 inline-flex items-center">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-10 w-10 mr-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 10h18M7 15h1m4 0h1m-7 4h12a3 3 0 003-3V8a3 3 0 00-3-3H6a3 3 0 00-3 3v8a3 3 0 003 3z" />
                                        </svg>
                                        Renew Subscription
                                    </button>
                                </form>
                            @else
                                <form action="{{ route('users.subscribe', auth()->id()) }}" method="post">
                                    @csrf
                                    <button type="submit" class="button button-pasific hover-ripple-out text-3xl antialiased font-normal tracking-wide mt-5 px-6 py-4 inline-flex items-center">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-10 w-10 mr-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 10h18M7 15h1m4 0h1m-7 4h12a3 3 0 003-3V8a3 3 0 00-3-3H6a3 3 0 00-3 3v8a3 3 0 003 3z" />
                                        </svg>
                                        Subscribe
                                    </button>
                                </form>
                            @endif
                        </div>
                    </div>
                @endif
				</div>
            </div>
        </div>

        @include('layouts.frontend.partials._footer')

        <!-- JQuery Core
        =====================================-->
        <script src="{{ asset("frontend/assets/js/core/jquery.min.js") }}"></script>
        <script src="{{ asset("frontend/assets/js/core/bootstrap-3.3.7.min.js") }}"></script>

        <!-- Magnific Popup
        =====================================-->
        <script src="{{ asset("frontend/assets/js/magnific-popup/jquery.magnific-popup.min.js") }}"></script>
        <script src="{{ asset("frontend/assets/js/magnific-popup/magnific-popup-zoom-gallery.js") }}"></script>

        <!-- JQuery Main
        =====================================-->
        <script src="{{ asset("frontend/assets/js/main/jquery.appear.js") }}"></script>
        <script src="{{ asset("frontend/assets/js/main/isotope.pkgd.min.js") }}"></script>
        <script src="{{ asset("frontend/assets/js/main/parallax.min.js") }}"></script>
        <script src="{{ asset("frontend/assets/js/main/jquery.sticky.js") }}"></script>
        <script src="{{ asset("frontend/assets/js/main/imagesloaded.pkgd.min.js") }}"></script>
        <script src="{{ asset("frontend/assets/js/main/main.js") }}"></script>

    </body>
</html>