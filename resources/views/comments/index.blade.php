@extends('layouts.admin-panel.app')

@section('title', 'WayToBlog')

@section('content')
	@if(!$comments->isEmpty())
        @foreach ($comments as $comment)
			<div class="card mb-4 py-2 shadow-sm">
				<div class="flex items-center">
                    <img src="{{ $comment->author->gravatar_image }}" class="rounded-full m-3" width="60px" alt="image">
                    <div class="text-base pr-4 border-r border-gray-300 w-1/6">
                        <div class="leading-tight">{{ $comment->author->name }}</div>
                        <a href="{{ route('blogs.preview', $comment->post_id) }}" class="flex items-center text-gray-500 hover:text-red-500" target="_blank">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-3 w-3 mr-1" viewBox="0 0 20 20" fill="currentColor">
                                    <path fill-rule="evenodd" d="M7.707 3.293a1 1 0 010 1.414L5.414 7H11a7 7 0 017 7v2a1 1 0 11-2 0v-2a5 5 0 00-5-5H5.414l2.293 2.293a1 1 0 11-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clip-rule="evenodd" />
                            </svg> on this post
                        </a>
                        <span class="text-sm text-muted">{{ $comment->created_at->diffForHumans() }}</span>
                    </div>
                    <p class="text-xl self-center mx-4 mb-1">
                        {{ $comment->comment }}
                    </p>
                    @if (auth()->user()->isAdmin() && (!$comment->isApproved()))
                        <form action="{{ route('comments.approve', $comment->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <button type="submit" class="btn btn-outline-success flex mr-3">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                </svg>
                                Approve
                            </button>
                        </form>
                    @elseif (auth()->user()->isAdmin() && $comment->isApproved())
                        <form action="{{ route('comments.disapprove', $comment->id) }}" method="post">
                            @csrf
                            @method('PUT')
                            <button type="submit" class="btn btn-outline-danger flex mr-3">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                </svg>
                                Disapprove
                            </button>
                        </form>
                    @endif
                    @if($comment->isPending())
                        <div class="badge ml-2 self-center px-1 py-1 rounded-full text-sm text-white bg-yellow-500 flex leading-relaxed px-3">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
                            </svg>
                            Approval pending
                        </div>
                    @elseif($comment->isApproved())
                        <div class="badge ml-2 self-center px-1 py-1 rounded-full text-sm text-white bg-success flex leading-relaxed px-3"><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                            </svg>
                            Approved By {{ $comment->approver->name }}
                        </div>
                    @else
                        <div class="badge ml-2 self-center px-1 py-1 rounded-full text-sm text-white bg-danger flex leading-relaxed px-3">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                            </svg>Disapproved!
                        </div>
                    @endif
                </div>
				@if($comment->replies()->count() > 0 && auth()->user()->isAdmin())
					@foreach ($comment->replies as $reply)
                    <div class="mx-10 border-t border-gray-300 p-2">
						<div class="flex items-center">
                            <img src="{{ $reply->author->gravatar_image }}" class="rounded-full m-3" width="60px" alt="image">
                            <div class="text-base pr-4 border-r border-gray-300 w-1/6">
                                <div class="leading-3">{{ $reply->author->name }}</div>
                                <div class="flex items-center text-muted text-sm pt-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-3 w-3 mr-1" viewBox="0 0 20 20" fill="currentColor">
                                        <path fill-rule="evenodd" d="M7.707 3.293a1 1 0 010 1.414L5.414 7H11a7 7 0 017 7v2a1 1 0 11-2 0v-2a5 5 0 00-5-5H5.414l2.293 2.293a1 1 0 11-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clip-rule="evenodd" />
                                    </svg>{{ $comment->author->name }}
                                </div>
                                <span class="text-sm text-muted">{{ $reply->created_at->diffForHumans() }}</span>
                            </div>
                            <div class="text-xl self-center mx-4 mb-1">{{ $reply->comment }}</div>
                            @if (auth()->user()->isAdmin() && (!$reply->isApproved()))
                                <form action="{{ route('comments.approve', $reply->id) }}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <button type="submit" class="btn btn-outline-success flex mr-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                        Approve
                                    </button>
                                </form>
                            @elseif (auth()->user()->isAdmin() && $reply->isApproved())
                                <form action="{{ route('comments.disapprove', $reply->id) }}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <button type="submit" class="btn btn-outline-danger flex mr-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                        Disapprove
                                    </button>
                                </form>
                            @endif
                            @if($reply->isPending())
                                <div class="badge ml-2 self-center px-1 py-1 rounded-full text-sm text-white bg-yellow-500 flex leading-relaxed px-3">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
                                    </svg>
                                    Approval pending
                                </div>
                            @elseif($reply->isApproved())
                                <div class="badge ml-2 self-center px-1 py-1 rounded-full text-sm text-white bg-success flex leading-relaxed px-3"><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                    </svg>
                                    Approved By {{ $reply->approver->name }}
                                </div>
                            @else
                                <div class="badge ml-2 self-center px-1 py-1 rounded-full text-sm text-white bg-danger flex leading-relaxed px-3">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                    </svg>Disapproved!
                                </div>
                            @endif
                        </div>
                    </div>
					@endforeach
				@endif
			</div>
		@endforeach
        <div class="mt-5">
            {{ $comments->links('vendor.pagination.bootstrap-4') }}
        </div>
    @else
        <div class="text-2xl">you have not posted any comments yet.</div>
    @endif
@endsection