@extends('layouts.admin-panel.app')

@section('page-level-styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.min.css" integrity="sha512-5m1IeUDKtuFGvfgz32VVD0Jd/ySGX7xdLxhqemTmThxHdgqlgPdupWoSN8ThtUSLpAGBvA8DY2oO7jJCrGdxoA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section('title', 'WayToBlog')

@section('content')

    <div class="card">
        <div class="card-header h2">Add a Post</div>
        <div class="card-body">
            <form action="{{ route('posts.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="title" class="@error('title') text-danger @enderror" >Title</label>
                    <input type="text"
                            class="form-control @error('title') is-invalid @enderror"
                            id="title"
                            name="title"
                            value="{{ old('title') }}"
                            placeholder="Enter Post title">
                    @error('title')
                        <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="excerpt" class="@error('excerpt') text-danger @enderror" >Excerpt</label>
                    <input type="text"
                            class="form-control @error('excerpt') is-invalid @enderror"
                            id="excerpt"
                            name="excerpt"
                            value="{{ old('excerpt') }}"
                            placeholder="Enter Post excerpt">
                    @error('excerpt')
                        <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="content" class="@error('content') text-danger @enderror" >Content</label>
                    <input id="content" type="hidden" name="content">
                    <trix-editor input="content">{!! old('content') !!}</trix-editor>
                    @error('content')
                        <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="category_id" class="@error('category_id') text-danger @enderror" >Category</label>
                    <select type="text"
                            class="form-control select2 @error('category_id') is-invalid @enderror"
                            name="category_id">
                            <option></option>
                            @foreach ($categories as $category)
                                <option value="{{$category->id}}">{{ $category->name }}</option>
                            @endforeach
                    </select>
                    @error('category')
                        <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="tags">Tags</label>
                    <select name="tags[]" id="tags" class="form-control select2" multiple>
                        <option></option>

                        @foreach ($tags as $tag)
                           <option value="{{ $tag->id }}"
                                {{ (old('tags') && (in_array($tag->id, old('tags'))) ? 'selected' : '') }}>
                                {{$tag->name}}
                           </option>
                        @endforeach
                    </select>
                    @error('tags')
                        <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                @if(auth()->user()->isMember())

                    <label for="published_at">Publish At</label>
                    <div class="form-group flex">
                        <label class="mb-0 cursor-pointer flex items-center mr-4">
                            <input type="radio" value="now" name="publish" aria-label="Radio button for following text input" @if(old('publish') == 'now') checked @endif>
                            <div class="ml-2">Now</div>
                        </label>
                        <label class="mb-0 cursor-pointer flex items-center">
                            <input type="radio" name="publish" value="future" class="future" aria-label="Radio button for following text input" @if(old('publish') == 'future') checked @endif>
                            <div class="ml-2">Auto-publish in future</div>
                        </label>
                    </div>

                    <div id="publish-field" class="form-group">
                        @if (old('published_at'))
                            <input
                                type="text"
                                value="{{ old('published_at') }}"
                                class="form-control"
                                name="published_at"
                                id="published_at"
                                >
                            @error('published_at')
                                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        @endif
                    </div>
                @endif

                <div class="form-group">
                    <label for="image">Image</label>
                    <input
                        type="file"
                        class="form-control  @error('image') is-invalid @enderror"
                        name="image"
                        id="image"
                        >
                    @error('image')
                        <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <button type="submit" name="submit" class="btn btn-outline-success float-right px-4" value="submit">Add</button>
                <button type="submit" class="btn bg-red-500 text-gray-50 border-red-500 hover:bg-white hover:text-red-500 float-right mr-2">Save as draft</button>
            </form>
        </div>
    </div>

@endsection

@section('page-level-scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.min.js" integrity="sha512-2RLMQRNr+D47nbLnsbEqtEmgKy67OSCpWJjJM394czt99xj3jJJJBQ43K7lJpfYAYtvekeyzqfZTx2mqoDh7vg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        
        $('input[name="publish"]').click(function(event) {
            if($(event.target).hasClass('future')) {
                let data = `<input
                            type="text"
                            value="{{ old('published_at') }}"
                            class="form-control"
                            name="published_at"
                            id="published_at"
                            >
                        @error('published_at')
                            <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                        @enderror`;
                $('#publish-field').html(data);
                flatpickr('#published_at', {
                    enableTime : true,
                    dateFormat : "Y-m-d H:i",
                    minDate: new Date()
                });
            } else {
                $('#publish-field').html("");
            }
        });
        $('.select2').select2({
            placeholder: "Select a state",
            allowClear: true
        });
    </script>
@endsection