@extends('layouts.admin-panel.app')

@section('page-level-styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.min.css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section('title', 'WayToBlog')

@section('content')

    <div class="card">
        <div class="card-header h2">Edit a Post</div>
        <div class="card-body">
            <form action="{{ route('posts.update', $post->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="title" class="@error('title') text-danger @enderror" >Title</label>
                    <input type="text"
                            class="form-control @error('title') is-invalid @enderror"
                            id="title"
                            name="title"
                            value="{{ old('title', $post->title) }}"
                            placeholder="Enter Post title">
                    @error('title')
                        <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="excerpt" class="@error('excerpt') text-danger @enderror" >Excerpt</label>
                    <input type="text"
                            class="form-control @error('excerpt') is-invalid @enderror"
                            id="excerpt"
                            name="excerpt"
                            value="{{ old('excerpt', $post->excerpt) }}"
                            placeholder="Enter Post excerpt">
                    @error('excerpt')
                        <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="content" class="@error('content') text-danger @enderror" >Content</label>
                    <input id="content" type="hidden" name="content" value="{{ old('content', $post->content) }}">
                    <trix-editor input="content"></trix-editor>
                    @error('content')
                        <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="category_id">Category</label>
                    <select name="category_id" id="category_id" class="form-control select2">
                        @foreach ($categories as $category)
                            @if($category->id == old('category_id', $post->category_id))
                                <option value="{{ $category->id}}" selected>{{ $category->name }}</option>
                            @else
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('category_id')
                        <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="tags">Tags</label>
                    <select name="tags[]" id="tags" class="form-control select2" multiple>
                        <option></option>

                        @foreach ($tags as $tag)
                            @if((old('tags') && (in_array($tag->id, old('tags')))) || $post->hasTag($tag->id) )
                                <option value="{{ $tag->id }}" selected> {{$tag->name}} </option>
                            @else
                                <option value="{{ $tag->id }}"> {{$tag->name}} </option>
                            @endif
                        @endforeach
                    </select>
                    @error('tags')
                        <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>

                @if(auth()->user()->isMember())

                    <label for="published_at">Publish At</label>
                    <div class="form-group flex">
                        <label class="mb-0 cursor-pointer flex items-center mr-4">
                            <input type="radio" value="now" name="publish" aria-label="Radio button for following text input" @if(old('publish') == 'now') checked @endif>
                            <div class="ml-2">Now</div>
                        </label>
                        <label class="mb-0 cursor-pointer flex items-center">
                            <input type="radio" name="publish" value="future" class="future" aria-label="Radio button for following text input" @if(old('published_at', $post->published_at)) checked @endif>
                            <div class="ml-2">Auto-publish in future</div>
                        </label>
                    </div>

                    <div id="publish-field" class="form-group">
                        @if (old('published_at', $post->published_at))
                            <input
                                type="text"
                                value="{{ old('published_at', $post->published_at) }}"
                                class="form-control"
                                name="published_at"
                                id="published_at"
                                >
                            @error('published_at')
                                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        @endif
                    </div>
                @endif

                <div class="form-group">
                    <label for="image">Image</label>
                    <input
                        type="file"
                        class="form-control  @error('image') is-invalid @enderror"
                        name="image"
                        id="image">
                    <small id="emailHelp" class="form-text text-success mt-0">If you do not want to update image. leave the above field unchanged!</small>
                    @error('image')
                        <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <button type="submit" name="submit" class="btn btn-outline-success float-right px-4" value="submit">Update Changes</button>
                <button type="submit" class="btn bg-red-500 text-gray-50 border-red-500 hover:bg-white hover:text-red-500 float-right mr-2">Save as draft</button>
            </form>
        </div>
    </div>

@endsection

@section('page-level-scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        $('input[name="publish"]').click(function(event) {
            if($(event.target).hasClass('future')) {
                let data = `<input
                            type="text"
                            value="{{ old('published_at', $post->published_at) }}"
                            class="form-control"
                            name="published_at"
                            id="published_at"
                            >
                        @error('published_at')
                            <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                        @enderror`;
                $('#publish-field').html(data);
                flatpickr('#published_at', {
                    enableTime : true,
                    dateFormat : "Y-m-d H:i",
                    minDate: new Date()
                });
            } else {
                $('#publish-field').html("");
            }
        });
        $('.select2').select2({
            placeholder: "Select a state",
            allowClear: true
        });
        flatpickr('#published_at', {
            enableTime : true,
            dateFormat : "Y-m-d H:i",
            minDate: new Date()
        });
    </script>
@endsection