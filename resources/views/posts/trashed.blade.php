@extends('layouts.admin-panel.app')

@section('title', 'WayToBlog | Posts')

@section('content')

    @if(!$posts->isEmpty())
        @foreach ($posts as $post)
            <div class="row g-0 border rounded relative overflow-hidden flex-md-row mb-4 shadow-sm h-md-250">
                <div class="col-auto sm:w-px md:w-4/12 flex justify-center mx-auto align-center">
                    <img src="{{ asset($post->image_path) }}" class="rounded-xl object-fill img-fluid align-self-center sm:w-px md:w-full justify-self-center" width="382px">
                </div>
                <div class="col p-4 d-flex flex-column position-static">
                    <strong class="d-inline-block mb-2 text-primary"><a class="text-red-500 hover:text-red-600" href="{{route('blogs.categories', $post->category->id)}}">{{ $post->category->name }}</a></strong>

                    <h3 class="mb-1 h4 md:w-3/5">{{ $post->title }}</h3>
                    <div class="text-muted"><span class="text-gray-700">{{ $post->deleted_at->diffForHumans()}}</span> &ndash; {{ $post->author->name }}
                </div>
                <p class="card-text text-muted mb-2">{{ Str::limit($post->excerpt, (Str::length($post->excerpt)/2), '...') }}</p>
                <p class="card-text mb-3">{{ strip_tags(Str::limit($post->content, (Str::length($post->content)/5), '...')) }}</p>
                <div class="d-flex mt-2">
                    @if ($post->user_id == auth()->id())
                        <a href="#restoreModal" type="submit" class="btn text-red-500 border-red-500 hover:bg-red-500 hover:text-white flex mr-3" onclick="displayRestoreModal({{ $post->id }})" data-toggle="modal">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M16 15v-1a4 4 0 00-4-4H8m0 0l3 3m-3-3l3-3m9 14V5a2 2 0 00-2-2H6a2 2 0 00-2 2v16l4-2 4 2 4-2 4 2z" />
                            </svg>
                            Restore
                        </a>
                    @endif
                    <a href="{{ route('blogs.preview', $post->id) }}" class="btn text-red-500 border-red-500 hover:bg-red-500 hover:text-white d-inline-block mr-3">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z" />
                        </svg>
                    </a>
                    @if(auth()->user()->isAdmin())
                        <a href="#deleteModal" class="btn btn-danger" onclick="displayModal({{ $post->id }})" data-toggle="modal">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                            </svg>
                        </a>
                    @endif
                </div>
            </div>
        @endforeach
        <div class="mt-5">
            {{ $posts->links('vendor.pagination.bootstrap-4') }}
        </div>
    @else
        <div class="text-2xl">There are no posts here.</div>
    @endif

    <!-- Restore MODAL -->
    <div class="modal fade" id="restoreModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Restore</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you want to restore this post?</p>
            </div>
            <div class="modal-footer">
                <form action="" method="POST" id="restorepostForm">
                    @csrf
                    @method('PUT')
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn bg-red-500 text-gray-50 border-red-500 hover:bg-white hover:text-red-500">Restore</button>
                </form>
            </div>
            </div>
        </div>
    </div>

    @if(auth()->user()->isAdmin())
        <!-- DELETE MODAL -->
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>This post will be deleted permanantly. Do you want to proceed?</p>
                </div>
                <div class="modal-footer">
                    <form action="" method="POST" id="deletepostForm">
                        @csrf
                        @method('DELETE')
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-outline-danger" value="Delete">
                    </form>
                </div>
                </div>
            </div>
        </div>
    @endif

@endsection

@section('page-level-scripts')
    <script>
        function displayModal(postId) {
            var url = '/posts/' + postId;
            $('#deletepostForm').attr('action', url);
        }
        function displayRestoreModal(postId) {
            var url = '/posts/restore/' + postId;
            $('#restorepostForm').attr('action', url);
        }
    </script>
@endsection