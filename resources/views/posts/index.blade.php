@extends('layouts.admin-panel.app')

@section('title', 'WayToBlog')

@section('errors')
    @error('comment')
        {{ session()->flash('error', $errors->first('comment')) }}
    @enderror
@endsection

@section('content')

    <div class="d-flex justify-content-end align-items-center mb-3">
        <a href="{{ route('posts.create') }}" class="btn bg-red-500 text-gray-50 border-red-500 hover:bg-white hover:text-red-500 d-flex align-items-center">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
            </svg>
            Add Post
        </a>
    </div>
    @if(!$posts->isEmpty())
        @foreach ($posts as $post)
            <div class="row g-0 border rounded relative overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                @if($post->isPending())
                    <div class="badge ml-2 self-center md:absolute md:top-0 md:right-0 m-4 px-2 py-2 rounded-full text-sm text-yellow-800 bg-yellow-500 font-medium flex leading-relaxed px-3">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
                        </svg>
                        Approval pending
                    </div>
                @elseif($post->isApproved())
                    <div class="btn ml-2 self-center md:absolute md:top-0 md:right-0 m-4 px-2 py-2 rounded-full text-sm text-green-100 btn-success font-medium flex leading-relaxed px-3" @if($post->approver->id != 1) data-toggle="popover" data-placement="bottom" data-content="By {{ $post->approver->name }}" @endif><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>
                        @if($post->approver->id === 1)
                        Member approved
                        @else
                        Approved
                        @endif
                    </div>
                @else
                    <button onclick="displayCommentModal('{{ $post->disapprover_comment }}')" data-toggle="modal" data-target="#commentModal" class="btn ml-2 self-center md:absolute md:top-0 md:right-0 m-4 px-2 py-2 rounded-full text-sm text-white btn-danger font-medium flex leading-relaxed px-3">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>Disapproved!
                    </button>
                @endif
                <div class="col-auto sm:w-px md:w-5/12 flex justify-center mx-auto align-center">
                    <img src="{{ asset($post->image_path) }}" class="rounded-xl object-fill img-fluid align-self-center sm:w-px md:w-full justify-self-center" width="382px">
                </div>
                <div class="col p-4 d-flex flex-column position-static">
                <strong class="d-inline-block mb-2 text-primary">
                    <a class="text-red-500 hover:text-red-600" href="{{route('blogs.categories', $post->category->id)}}">{{ $post->category->name }}</a>
                </strong>

                <h3 class="mb-1 h4 md:w-3/5">{{ $post->title }}</h3>
                <div class="text-muted"><span class="text-gray-700">{{ $post->published_at > now()? 'Scheduled '.$post->published_at->format('jS F \\a\\t h:i') : 'Posted '.$post->published_at->diffForHumans() }}</span> &ndash; {{ 'By '.$post->author->name }}</div>
                <p class="card-text text-muted mt-2">{{ Str::limit($post->excerpt, (Str::length($post->excerpt)/2), '...') }}</p>
                <p class="card-text mb-3 text-gray-700">{{ strip_tags(Str::limit($post->content, (Str::length($post->content)/5), '...')) }}</p>
                <div class="d-flex flex-wrap  mt-2">
                @if (auth()->user()->isAdmin() && (!$post->isApproved()))
                    <form action="{{ route('posts.approve', $post->id) }}" method="post">
                        @csrf
                        @method('PUT')
                        <button type="submit" class="btn btn-outline-success flex mr-3">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                            </svg>
                            Approve
                        </button>
                    </form>
                @elseif (auth()->user()->isAdmin() && $post->isApproved())
                    <a href="#disapproveModal" class="btn btn-outline-danger flex mr-3" data-toggle="modal" onclick="displayDisapproveModal({{ $post->id }})">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>
                        Disapprove
                    </a>
                @endif

                <a href="{{ route('blogs.preview', $post->id) }}" class="btn btn-outline-primary d-inline-block mr-3">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z" />
                    </svg>
                </a>
                
                @if($post->user_id == auth()->id())
                    <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-outline-primary d-inline-block mr-3">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                        </svg>
                    </a>
                    <a href="#deleteModal" class="btn btn-outline-danger" onclick="displayDeleteModal({{ $post->id }})" data-toggle="modal">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                        </svg>
                    </a>
                @endif
                </div>
                </div>
            </div>
        @endforeach
    @else
        <div class="text-2xl">There are no posts here.</div>
    @endif
    <div class="mt-5">
        {{ $posts->links('vendor.pagination.bootstrap-4') }}
    </div>

    <!-- Comment Modal -->
    <div class="modal fade" id="commentModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header text-danger">
                <span>
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </span>
                <h5 class="modal-title ml-2">Disapproved</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="mb-2 comment-text"></p>
            </div>
            </div>
            </div>
        </div>
    </div>

    <!-- Disapprove Modal -->
    <div class="modal fade" id="disapproveModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header text-danger">
                <span>
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </span>
                <h5 class="modal-title ml-2">Disapprove</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="mb-2">Why are you disapproving this post?</p>
                <form action="" method="post" id="disapproveForm">
                <textarea
                    type="text"
                    name="comment"
                    class="form-control"
                    placeholder="Leave a comment."
                ></textarea>
            </div>
            <div class="modal-footer">
                    @csrf
                    @method('PUT')
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-outline-danger" value="Disapprove">
                </form>
            </div>
            </div>
        </div>
    </div>

    <!-- DELETE MODAL -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Do you wish to delete this post?</p>
            </div>
            <div class="modal-footer">
                <form action="" method="POST" id="deletepostForm">
                    @csrf
                    @method('DELETE')
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-outline-danger" value="Move to bin">
                </form>
            </div>
            </div>
        </div>
    </div>

@endsection

@section('page-level-scripts')
    <script>
        $(function () {
          $('[data-toggle="popover"]').popover()
        });
        function displayCommentModal(comment) {
            $('.comment-text').html(comment);
        };
        function displayDeleteModal(postId) {
            var url = '/posts/trash/' + postId;
            $('#deletepostForm').attr('action', url);
        };
        function displayDisapproveModal(postId) {
            var url = '/posts/disapprove/' + postId;
            $('#disapproveForm').attr('action', url);
        };
    </script>
@endsection