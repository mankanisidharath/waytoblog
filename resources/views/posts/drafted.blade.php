@extends('layouts.admin-panel.app')

@section('title', 'WayToBlog')

@section('content')

    @if(!$posts->isEmpty())
        @foreach ($posts as $post)
            <div class="row g-0 border rounded relative overflow-hidden flex-md-row mb-4 shadow-sm h-md-250">
                <div class="col-auto sm:w-px md:w-4/12 flex justify-center mx-auto align-center">
                        <img src="{{ asset($post->image_path) }}" class="rounded-xl object-fill img-fluid align-self-center sm:w-px md:w-full justify-self-center" width="382px">
                    </div>
                    <div class="col p-4 d-flex flex-column position-static">
                    <strong class="d-inline-block mb-2"><a class="text-red-500 hover:text-red-600" href="{{route('blogs.categories', $post->category->id)}}">{{ $post->category->name }}</a></strong>

                    <h3 class="mb-1 h4 md:w-3/5">{{ $post->title }}</h3>
                    <div class="text-muted">By {{ $post->author->name }}</div>
                    <p class="card-text text-muted mb-2">{{ Str::limit($post->excerpt, (Str::length($post->excerpt)/2), '...') }}</p>
                    <p class="card-text mb-3">{{ strip_tags(Str::limit($post->content, (Str::length($post->content)/4), '...')) }}</p>
                    <div class="d-flex mt-2">
                        <a href="#publishModal" type="submit" class="btn text-red-500 border-red-500 hover:bg-red-500 hover:text-white flex items-center mr-3" onclick="displayPublishModal({{ $post->id }})" data-toggle="modal">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 mr-2" viewBox="0 0 20 20" fill="currentColor">
                                <path fill-rule="evenodd" d="M3.293 9.707a1 1 0 010-1.414l6-6a1 1 0 011.414 0l6 6a1 1 0 01-1.414 1.414L11 5.414V17a1 1 0 11-2 0V5.414L4.707 9.707a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                            </svg>
                            Publish
                        </a>
                        <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-outline-primary d-inline-block mr-3">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                            </svg>
                        </a>
                        <a href="#deleteModal" class="btn btn-danger" onclick="displayDeleteModal({{ $post->id }})" data-toggle="modal">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                            </svg>
                        </a>
                    </div>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="mt-5">
            {{ $posts->links('vendor.pagination.bootstrap-4') }}
        </div>
    @else
        <div class="text-2xl">There are no posts here.</div>
    @endif
    

    <!-- Publish MODAL -->
    <div class="modal fade" id="publishModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Publish Post</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you want to publish this post?</p>
            </div>
            <div class="modal-footer">
                <form action="" method="POST" id="publishpostForm">
                    @csrf
                    @method('PUT')
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn bg-red-500 text-gray-50 border-red-500 hover:bg-white hover:text-red-500">Publish</button>
                </form>
            </div>
            </div>
        </div>
    </div>

    <!-- DELETE MODAL -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>This post will move to bin. Do you want to proceed?</p>
            </div>
            <div class="modal-footer">
                <form action="" method="POST" id="deletepostForm">
                    @csrf
                    @method('DELETE')
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn bg-red-500 text-gray-50 border-red-500 hover:bg-white hover:text-red-500" value="Move to bin">
                </form>
            </div>
            </div>
        </div>
    </div>

@endsection

@section('page-level-scripts')
    <script>
        function displayDeleteModal(postId) {
            var url = '/posts/trash/' + postId;
            $('#deletepostForm').attr('action', url);
        }
        function displayPublishModal(postId) {
            var url = '/posts/publishDrafted/' + postId;
            $('#publishpostForm').attr('action', url);
        }
    </script>
@endsection