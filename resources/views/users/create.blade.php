@extends('layouts.admin-panel.app')

@section('title', 'WayToBlog')

@section('content')

    <div class="card">
        <div class="card-header h2">Add User</div>
        <div class="card-body">
            <form action="{{ route('users.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name" class="@error('name') text-danger @enderror" >Name</label>
                    <input  type="text"
                            class="form-control @error('name') is-invalid @enderror"
                            id="name"
                            name="name"
                            value="{{ old('name') }}"
                            placeholder="Enter Name">
                    @error('name')
                        <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="email" class="@error('email') text-danger @enderror" >Email</label>
                    <input  type="text"
                            class="form-control @error('email') is-invalid @enderror"
                            id="email"
                            name="email"
                            value="{{ old('email') }}"
                            placeholder="Enter email">
                    @error('email')
                        <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="password" class="@error('password') text-danger @enderror" >Password</label>
                    <input  type="password"
                            class="form-control @error('password') is-invalid @enderror"
                            id="password"
                            name="password"
                            value="{{ old('password') }}"
                            placeholder="Enter password">
                    @error('password')
                        <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <button type="submit" class="btn btn-outline-success float-right px-4">Add</button>
            </form>
        </div>
    </div>

@endsection
