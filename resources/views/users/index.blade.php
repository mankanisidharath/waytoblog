@extends('layouts.admin-panel.app')

@section('title', 'WayToBlog | Users')

@section('content')
    <div class="d-flex justify-content-end align-items-center mb-3">
        <a href="{{ route('users.create') }}" class="btn bg-red-500 text-gray-50 border-red-500 hover:bg-white hover:text-red-500 d-flex align-items-center">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
            </svg>
            Add User</a>
    </div>
    <table class="table table-hover">
        <thead>
            <tr>
            <th scope="col">Image</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col" class="text-center">Posts</th>
            <th scope="col" class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
                <tr>
                    <td><img src="{{asset("$user->gravatar_image")}}" class="rounded-circle"></td>
                    <td style="line-height: 80px;">
                        {{ $user->name }}
                    </td>
                    <td style="line-height: 80px;">{{ $user->email }}</td>
                    <td class="text-center" style="line-height: 80px;">{{ $user->posts->count() }}</td>
                    <td class="text-center" style="line-height: 80px;">
                        @if(Auth::user()->id == $user->id)
                            <a href="{{ route('users.edit', $user->id) }}" class="btn text-red-500 border-red-500 hover:bg-red-500 hover:text-white">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                                </svg>
                            </a>
                        @endif
                        @if(!$user->isAdmin())
                            <form action="{{route('users.make-admin', $user->id)}}" method="POST">
                                @csrf
                                @method('PUT')
                                <button type="submit" class="btn btn-outline-success">Make Admin</button>
                            </form>
                        @elseif(Auth::user()->id != $user->id)
                            <form action="{{route('users.revoke-admin', $user->id)}}" method="POST">
                                @csrf
                                @method('PUT')
                                <button type="submit" class="btn btn-outline-danger">Revoke Admin</button>
                            </form>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="mt-5">
        {{$users->links('vendor.pagination.bootstrap-4')}}
    </div>
@endsection
