@extends('layouts.admin-panel.app')

@section('title', 'WayToBlog')

@section('content')

    <div class="card">
        <div class="card-header h2">Edit Profile</div>
        <div class="card-body">
            <form action="{{ route('users.update', $user->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name" class="@error('name') text-danger @enderror" >Name</label>
                    <input  type="text"
                            class="form-control @error('name') is-invalid @enderror"
                            id="name"
                            name="name"
                            value="{{ old('name', $user->name) }}"
                            placeholder="Enter Name">
                    @error('name')
                        <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="email" class="@error('email') text-danger @enderror" >Email</label>
                    <input  type="text"
                            class="form-control @error('email') is-invalid @enderror"
                            id="email"
                            name="email"
                            value="{{ old('email', $user->email) }}"
                            placeholder="Enter email">
                    @error('email')
                        <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="password" class="@error('password') text-danger @enderror" >Password</label>
                    <input  type="password"
                            class="form-control is-valid @error('password') is-invalid @enderror"
                            id="password"
                            name="password"
                            value="{{ old('password') }}"
                            placeholder="Enter password">
                    @error('password')
                        <small class="form-text text-danger password-error">{{ $message }}</small>
                    @enderror
                        <small class="form-text text-success password-text">Leave the field empty if you do not wish to change password!</small>
                </div>
                <button type="submit" class="btn btn-outline-success float-right px-4">Update Changes!</button>
            </form>
        </div>
    </div>

@endsection

@section('page-level-scripts')
    <script>
        
        if($('.password-error').length > 0) {
            $('.password-text').css('display', 'none');
        }
    </script>    
@endsection