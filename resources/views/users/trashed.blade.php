@extends('layouts.admin-panel.app')

@section('title', 'WayToBlog')

@section('content')

    @if(!$users->isEmpty())
        <table class="table table-hover">
            <thead class="thead-light">
                <tr>
                    <th scope="col">Image</th>	
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Posts</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                <tr style="height: 80px;">
                    <th scope="row">
                        <img src="{{ asset($user->gravatar_image) }}" class="rounded-circle">
                    </td>
                    <th scope="row" style="line-height: 80px;">{{ $user->name }}</td>
                    <th scope="row" style="line-height: 80px;">{{ $user->email }}</td>
                    <th scope="row" style="line-height: 80px;">{{ $user->posts->count() }}</td>
                    <td style="line-height: 80px;">
                            <a href="#restoreModal" type="submit" class="btn btn-outline-primary d-inline-block mr-2" onclick="displayRestoreModal({{ $user->id }})" data-toggle="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2 d-inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M16 15v-1a4 4 0 00-4-4H8m0 0l3 3m-3-3l3-3m9 14V5a2 2 0 00-2-2H6a2 2 0 00-2 2v16l4-2 4 2 4-2 4 2z" />
                                </svg>
                                Restore
                            </a>
                            <a href="#deleteModal" class="btn btn-outline-danger ml-2" onclick="displayModal({{ $user->id }})" data-toggle="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                </svg>
                            </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="mt-5">
            {{ $users->links('vendor.pagination.bootstrap-4') }}
        </div>
    @else
        <div class="text-2xl">There are no users here.</div>
    @endif
    

    <!-- Restore MODAL -->
    <div class="modal fade" id="restoreModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Restore</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you want to restore this user?</p>
            </div>
            <div class="modal-footer">
                <form action="" method="POST" id="restoreuserForm">
                    @csrf
                    @method('PUT')
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn bg-red-500 text-gray-50 border-red-500 hover:bg-white hover:text-red-500">Restore</button>
                </form>
            </div>
            </div>
        </div>
    </div>

    <!-- DELETE MODAL -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>This user will be deleted permanantly. Do you want to proceed?</p>
            </div>
            <div class="modal-footer">
                <form action="" method="POST" id="deleteuserForm">
                    @csrf
                    @method('DELETE')
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-outline-danger" value="Delete">
                </form>
            </div>
            </div>
        </div>
    </div>

@endsection

@section('page-level-scripts')
    <script>
        function displayModal(userId) {
            var url = '/users/' + userId;
            $('#deleteuserForm').attr('action', url);
        }
        function displayRestoreModal(userId) {
            var url = '/users/restore/' + userId;
            $('#restoreuserForm').attr('action', url);
        }
    </script>
@endsection