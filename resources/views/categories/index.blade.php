@extends('layouts.admin-panel.app')

@section('title', 'WayToBlog')

@section('content')

    <div class="d-flex justify-content-end align-items-center mb-3">
        <a href="{{ route('categories.create') }}" class="btn bg-red-500 text-gray-50 border-red-500 hover:bg-white hover:text-red-500 d-flex align-items-center">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
            </svg>
            Add Category
        </a>
    </div>
    @if(!$categories->isEmpty())
        <table class="table table-hover">
            <thead>
                <tr>
                <th scope="col">Category</th>
                <th scope="col">Posts</th>
                <th scope="col">Status</th>
                @if(auth()->user()->isAdmin())
                    <th scope="col">Actions</th>
                @endif
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $category)
                <tr>
                    <th scope="row">{{ $category->name }}</td>
                    <th scope="row">{{ $category->posts->count() }}</td>
                    <th scope="row">
                        @if($category->isPending())
                    <div class="text-sm text-yellow-500 inline-flex align-middle">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
                        </svg>
                        Approval pending
                    </div>
                @elseif($category->isApproved())
                    <div class="text-sm text-success inline-flex"><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>Approved</div>
                @else
                    <div class="text-sm text-danger inline-flex">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>Disapproved</div>
                @endif    
                    </td>
                    @if(auth()->user()->isAdmin())
                        <td class="flex">
                            @if (auth()->user()->isAdmin() && (!$category->isApproved()))
                                <form action="{{ route('categories.approve', $category->id) }}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <button type="submit" class="btn btn-outline-success flex mr-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                        Approve
                                    </button>
                                </form>
                            @elseif (auth()->user()->isAdmin() && $category->isApproved())
                                <form action="{{ route('categories.disapprove', $category->id) }}" method="post">    
                                    @csrf
                                    @method('PUT')
                                    <button type="submit" class="btn btn-outline-danger flex mr-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                        Disapprove
                                    </button>
                                </form>
                            @endif
                            <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-outline-primary mr-3">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                                </svg>
                            </a>
                            <a href="#deleteModal" class="btn btn-outline-danger" onclick="displayModal({{ $category->id }})" data-toggle="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.4" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                </svg>
                            </a>
                        </td>
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="mt-5">
            {{ $categories->links('vendor.pagination.bootstrap-4') }}
        </div>
    @else
        <div class="text-2xl">There are no categories here.</div>
    @endif

    <!-- DELETE MODAL -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you want to delete this category?</p>
            </div>
            <div class="modal-footer">
                <form action="" method="POST" id="deleteCategoryForm">
                    @csrf
                    @method('DELETE')
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn bg-red-500 text-gray-50 border-red-500 hover:bg-white hover:text-red-500" value="Delete category">
                </form>
            </div>
            </div>
        </div>
    </div>

@endsection

@section('page-level-scripts')
    <script>
        function displayModal(categoryId) {
            var url = '/categories/' + categoryId;
            $('#deleteCategoryForm').attr('action', url);
        }
    </script>
@endsection