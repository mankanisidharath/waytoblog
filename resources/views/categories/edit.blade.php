@extends('layouts.admin-panel.app')

@section('title', 'WayToBlog')

@section('content')

    <div class="card">
        <div class="card-header h2">Edit a category</div>
        <div class="card-body">
            <form action="{{ route('categories.update', $category->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name" class="@error('name') text-danger @enderror" >Name</label>
                    <input  type="text"
                            class="form-control @error('name') is-invalid @enderror"
                            id="name"
                            name="name"
                            value="{{ old('name', $category->name) }}"
                            placeholder="Enter Category Name">
                    @error('name')
                        <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <button type="submit" class="btn btn-outline-primary float-right px-4">Edit</button>
            </form>
        </div>
    </div>

@endsection
