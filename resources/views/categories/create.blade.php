@extends('layouts.admin-panel.app')

@section('title', 'WayToBlog')

@section('content')

    <div class="card">
        <div class="card-header h2">Add a category</div>
        <div class="card-body">
            <form action="{{ route('categories.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name" class="@error('name') text-danger @enderror" >Name</label>
                    <input  type="text"
                            class="form-control @error('name') is-invalid @enderror"
                            id="name"
                            name="name"
                            value="{{ old('name') }}"
                            placeholder="Enter Category Name">
                    @error('name')
                        <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <button type="submit" class="btn btn-outline-success float-right px-4">Add</button>
            </form>
        </div>
    </div>

@endsection
