<div class="blog-post-comment-container p-4">
	<h5><i class="fa fa-comments-o mb25 mr-2"></i> {{ $post->comments->count() }} Comments</h5>
	
	@if($post->comments->count() > 0)
		@foreach ($post->comments as $comment)
			<div class="blog-post-comment">
				<img src="{{ $comment->author->gravatar_image }}" class="img-circle" width="60px" alt="image">
				<span class="blog-post-comment-name text-2xl">{{ $comment->author->name }}</span> {{ $comment->created_at->diffForHumans() }}
				<a onclick="onReply({{ $comment->id }}, `{{ Str::limit($comment->comment, 80, '...')}}`, '{{ $comment->author->name }}')" class="pull-right text-gray text-xl cursor-pointer"><i class="fa fa-comment mr-2"></i> Reply</a>
				<p class="text-3xl pb-4">
					{{ $comment->comment }}
				</p>	

				@if($comment->approvedReplies()->count() > 0)
					@foreach ($comment->approvedReplies() as $reply)
						<div class="blog-post-comment-reply">
							<img src="{{ $reply->author->gravatar_image }}" class="img-circle" width="60px" alt="image">
							<span class="blog-post-comment-name text-2xl">{{ $reply->author->name }}</span> {{ $reply->created_at->diffForHumans() }}
							<a onclick="onReply({{ $comment->id }}, `{{ Str::limit($reply->comment, 80, '...')}}`, '{{ $reply->author->name }}')" class="pull-right text-gray cursor-pointer"><i class="fa fa-comment mr-2"></i> Reply</a>
								<div class="flex items-center mt-0 text-gray-400">
									<svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-2" viewBox="0 0 20 20" fill="currentColor">
									<path fill-rule="evenodd" d="M7.707 3.293a1 1 0 010 1.414L5.414 7H11a7 7 0 017 7v2a1 1 0 11-2 0v-2a5 5 0 00-5-5H5.414l2.293 2.293a1 1 0 11-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clip-rule="evenodd" />
									</svg>{{ $comment->author->name }}
								</div>
								<div class="text-3xl py-2">{{ $reply->comment }}</div>
						</div>
					@endforeach
				@endif
			</div>
		@endforeach
	@endif

	<div class="blog-post-leave-comment @if(!auth()->check()) cursor-not-allowed relative p-10 @endif">
		@if(!auth()->check())
			<div class="absolute center flex transition-all top-0 right-0 left-0 bottom-0 bg-gray-300 bg-opacity-70 h4 justify-center items-center p-4 text-danger">
				<a href="{{ route('login') }}" class="link-danger">You need to be logged in to post comments.</a>
			</div>
		@endif
		<h5><i class="fa fa-comment mt25 mb25 mr-2"></i> Leave Comment</h5>

		<form action="{{ route('comments.store') }}" method="POST" id="commentForm">
			@csrf
			<input type="hidden" name="post_id" id="post_id" value="{{$post->id}}">
			<div id="reply"></div>
			<textarea id="comment" name="comment" class="blog-leave-comment-textarea text-3xl" placeholder="Write here..."></textarea>
			<div class="color-pasific text-2xl">{{ session()->get('posted') }}</div>
			<button type="submit" id="submit" class="button button-pasific button-sm center-block mb25 mt-6">Post Comment</button>
		</form>

	</div>

</div>

<script>
	function onReply(commentId, commentText, commentAuthor) {
		
		var el = `<input type="hidden" name="reply_to" value="${commentId}">
				<a class="text-2xl cursor-pointer inline-block" onClick="onCancelReply()">
					<div class="flex items-center">
						<svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-2" viewBox="0 0 20 20" fill="currentColor">
							<path fill-rule="evenodd" d="M7.707 3.293a1 1 0 010 1.414L5.414 7H11a7 7 0 017 7v2a1 1 0 11-2 0v-2a5 5 0 00-5-5H5.414l2.293 2.293a1 1 0 11-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clip-rule="evenodd" />
						</svg>${commentText}
					</div>
					<div class="flex items-center">Replying to ${commentAuthor}
						<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 ml-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
							<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
						</svg>
					</div>
				</a>`;
		$('#reply').html(el);
		$('#submit').html('Reply');
	}

	function onCancelReply() {
		$('#reply').html("");
		$('#submit').html('Post comment');
	}
</script>