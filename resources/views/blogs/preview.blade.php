@extends('layouts.frontend.layout')

@section('title', 'WayToBlog')

{{ session()->flash('preview', 'preview') }}

@section('header')
	<header class="pt100 pb100 parallax-window-2" data-parallax="scroll" data-speed="0.5" data-image-src="{{ asset('frontend/assets/img/bg/img-bg-1.jpg')}}" data-positiony="1000">
		<div class="intro-body text-center">
			<div class="container">
				<div class="row">
					<div class="col-md-12 pt50">
						<div class="h1 brand-heading font-montserrat text-uppercase color-light" data-in-effect="fadeInDown">
							Way To Blog
							<small class="color-light alpha7">Complete Blogging Application</small>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
@endsection

@section('main-content')
	<div class="blog-three-mini">
		<div class="color-dark h2">{{ $post->title }}</div>
		<div class="h4">{{ $post->excerpt }}</div>

		<div class="blog-three-attrib">
			<div><i class="fa fa-calendar"></i>{{ $post->published_at->diffForHumans() }}</div> |
			<div><i class="fa fa-pencil"></i>{{ $post->author->name }}</div> |
			<div><i class="fa fa-comment-o"></i>{{ $post->comments->count() }} Comments</div> |
			<div><a href=""><i class="fa fa-thumbs-o-up"></i></a>150 Likes</div> |
			<div>
				Share:  <a href=""><i class="fa fa-facebook-official"></i></a>
				<a href=""><i class="fa fa-twitter"></i></a>
				<a href=""><i class="fa fa-linkedin"></i></a>
				<a href=""><i class="fa fa-google-plus"></i></a>
				<a href=""><i class="fa fa-pinterest"></i></a>
			</div>
		</div>

		<img src="{{ asset($post->image_path) }}" alt="Blog Image" class="img-responsive mb50">
		
		<div>
			{!! $post->content !!}
		</div>
		<div class="blog-post-read-tag mt50 mb50">
			<i class="fa fa-tags"></i> Tags:
			@foreach ($post->tags as $tag)
				<a href="">{{ $tag->name }}</a>{{$loop->last ? '': ','}}
			@endforeach
		</div>

		<div class="blog-post-author bt-solid-1 pt30 mb-5">
			<img src="{{ $post->author->gravatar_image }}" class="img-circle" alt="image">
			<span class="blog-post-author-name h4">By {{ $post->author->name }} <a href="https://twitter.com/booisme"><i class="fa fa-twitter"></i></a></span>
			<p>
				Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.
			</p>
		</div>
		<div class="blog-post-leave-comment">
			<h5><i class="fa fa-comment mt25 mb25 mr-2"></i> Leave Comment</h5>
				<textarea class="blog-leave-comment-textarea text-3xl" placeholder="Write here..."></textarea>
				<button type="submit" id="submit" class="button button-pasific button-sm center-block mb25 mt-6">Post Comment</button>
		</div>
	</div>
@endsection