@if ($paginator->hasPages())
    <nav>
        {{-- Previous Page Link --}}
        <div class="row mt25 animated" data-animation="fadeInUp" data-animation-delay="100">
            <div class="col-md-6">
                @if ($paginator->onFirstPage())
                    <button style="background-color: #E0E0E0!important;color: #111!important;" class="button button-sm button-pasific pull-left">
                        Previous page
                    </button>
                @else
                    <a href="{{ $paginator->previousPageUrl() }}" class="button button-sm button-pasific pull-left hover-skew-backward">
                        Previous page
                    </a>
                @endif
            </div>

            <div class="col-md-6">
                {{-- Next Page Link --}}
                @if ($paginator->hasMorePages())
                    <a href="{{ $paginator->nextPageUrl() }}" class="button button-sm button-pasific pull-right hover-skew-forward">Next Page</a>
                @else
                    <button style="background-color: #E0E0E0!important;color: #111!important;" class="button button-sm button-pasific pull-right">Next Page</button>
                @endif
            </div>
        </div>
    </nav>
@endif