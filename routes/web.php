<?php

use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\CommentsController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\TagsController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// FrontEndController

Route::get('/', [FrontendController::class, 'index'])->name('home');
Route::get('/subscription', [FrontendController::class, 'subscription'])->name('subscription')->middleware('auth');
Route::get('/blogs/{post}', [FrontendController::class, 'show'])->name('blogs.show')->middleware('verifyPreviewPost');
Route::get('/blogs/{post}/preview', [FrontendController::class, 'preview'])->name('blogs.preview')->middleware('verifyPreviewPost');
Route::get('/blogs/categories/{category}', [FrontendController::class, 'categories'])->name('blogs.categories');
Route::get('/blogs/tags/{tag}', [FrontendController::class, 'tags'])->name('blogs.tags');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

// Application Routes
Route::middleware(['auth'])->group(function() {

    Route::put('categories/approve/{category}', [CategoriesController::class, 'approve'])->name('categories.approve');
    Route::put('categories/disapprove/{category}', [CategoriesController::class, 'disApprove'])->name('categories.disapprove');
    Route::resource('categories', CategoriesController::class);
    
    Route::put('tags/approve/{tag}', [TagsController::class, 'approve'])->name('tags.approve');
    Route::put('tags/disapprove/{tag}', [TagsController::class, 'disApprove'])->name('tags.disapprove');
    Route::resource('tags', TagsController::class);
    
    Route::get('comments/', [CommentsController::class, 'index'])->name('comments.index');
    Route::post('comments/', [CommentsController::class, 'store'])->name('comments.store');
    Route::put('comments/approve/{comment}', [CommentsController::class, 'approve'])->name('comments.approve');
    Route::put('comments/disapprove/{comment}', [CommentsController::class, 'disApprove'])->name('comments.disapprove');

    Route::delete('posts/trash/{post}', [PostsController::class, 'trash'])->name('posts.trash');
    Route::get('posts/drafted', [PostsController::class, 'drafted'])->name('posts.drafted');
    Route::put('posts/publishDrafted/{post}', [PostsController::class, 'publishDrafted'])->name('posts.publishDrafted');
    Route::get('posts/trashed', [PostsController::class, 'trashed'])->name('posts.trashed');
    Route::put('posts/restore/{post}', [PostsController::class, 'restore'])->name('posts.restore');
    Route::put('posts/approve/{post}', [PostsController::class, 'approve'])->name('posts.approve');
    Route::put('posts/disapprove/{post}', [PostsController::class, 'disApprove'])->name('posts.disapprove');
    Route::resource('posts', PostsController::class);
    
    Route::delete('users/trash/{user}', [UsersController::class, 'trash'])->name('users.trash');
    Route::put('users/restore/{user}', [UsersController::class, 'restore'])->name('users.trashed');
    Route::get('users/trashed', [UsersController::class, 'trashed'])->name('users.trashed');
    Route::put('users/make-admin/{user}', [UsersController::class, 'makeAdmin'])->name('users.make-admin');
    Route::put('users/revoke-admin/{user}', [UsersController::class, 'revokeAdmin'])->name('users.revoke-admin');
    Route::post('users/{user}/subscribe', [UsersController::class, 'subscribe'])->name('users.subscribe');
    Route::put('users/{user}/renew-subscription', [UsersController::class, 'renewSubscription'])->name('users.renewSubscription');
    Route::resource('users', UsersController::class);
});