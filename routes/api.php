<?php
use Illuminate\Support\Facades\Route;

Route::get('/postsJson', [\App\Http\Controllers\PostsController::class, 'getPostsJson'])->name('getPostsJson');
